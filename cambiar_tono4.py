from __future__ import division
import cv2
import numpy as np
import click_crop as cc
import math

# para cambiar la imagen ir a linea 109

def normalizar(lista):
    lista[2]/255.0, lista[1]/255.0, lista[0]/255.0

def calcularRGB2HSI(lista):
    #las imagenes estan almacenadas en BGR, dividir float y no enteros
    R, G, B =  lista[2]/255.0, lista[1]/255.0, lista[0]/255.0
    intensidad = calcularI(lista)
    r,g,b = R/intensidad, G/intensidad, B/intensidad

    if ((R==G) and (G==B)): 
        saturacion = 0
        tono = 0
    else:
        w = 0.5*(r-g +r-b)/math.sqrt((r-g)**2 + (r-b)*(g-b))
             
        if (w>1):
            w = 1
        if (w<-1):
            w = -1
        tono = math.acos(w)
        tono = tono* 180 / math.pi
        if  (tono<0):
            print("h negativo")
        if (B>G):
            tono = 360-tono
        if (r<= g) and (r<=b):
            saturacion = 1-3*r
        if (g<=r) and (g<=b):
            saturacion = 1-3*g
        if (b<= r) and (b<=g):
            saturacion = 1-3*b
        tono = int(round(tono,0))
        saturacion = round(saturacion,3)
        intensidad = round(intensidad,3)
    return [tono,saturacion,intensidad]

def calcularHSI2RGB(lista):
    H,S,I = lista[0],lista[1],lista[2]

    r,b,g = 0,0,0
    if (S>1):
        S = 1
    if (I>1):
        I = 1
    if (S==0):
        R,G,B=I,I,I
    elif ((H>=0) and (H<120)):
        b = (1 - S)/3
        r = 1/3*(1 + S * math.cos(H)/math.cos(60-H))
        g = 1 - b - r
    elif ((H>=120) and (H<240)):
        r = (1 - S)/3
        g = (1 + S * math.cos(H-120)/math.cos(180-H))/3
        b = 1 - r - g
    else: 
        g = (1 - S)/3
        b = (1 + S * math.cos(H-240)/math.cos(300-H))/3
        r = 1 - b - g
    """
    if (r<0):
        r = 0
    if (g<0):
        g = 0
    if (b<0):
        b = 0
    """

    #B,G,R = 3*I*b,3*I*g,3*I*r
    if ((H>=0) and (H<120)):
        B = int(round(I*b*255,0))
        R = int(round(I*r*255,0))+10
        if R>255: 
            R=255
        G = int(round(I*g*-255,0))+11
        if G>255: 
            G = 255
    elif ((H>=120) and (H<240)):
        R = int(round(255*I*r,0))
        G = int(round(255*I*g,0))+12
        if G>255: 
            G = 255
        B = int(round(-255*I*b,0))+11
        if B>255: 
            B = 255
    else:
        G = int(round(255*I*g,0))
        B = int(round(255*I*b,0))+13
        if B>255: 
            B = 255
        R = int(round(-255*I*r,0))
        if R>255: 
            R=255  


    return [B,G,R]
 
def calcularI(lista):
    return (lista[2]/255.0 + lista[1]/255.0 + lista[0]/255.0)/3.0

#cargar imagen
img =  cv2.imread("mouse.jpg")

#permite seleccionar un pedazo en la imagen y la guarda en pedazo.jpg
#Descargar el archivo click_crop.py
cc.crop(img.copy())

img2 =cv2.imread("pedazo.jpg")
cv2.imshow("pedazo", img2)
n,m,c = img2.shape

#buscar tono minimo y maximos
tono_max,tono_min = 0, 400

for i in range(0,n,1):
    for j in range(0,m,1):
		#transformacion de tono
        hsi = calcularRGB2HSI(img2[i][j])
        print("hsi= ",hsi[0],hsi[1],hsi[2])

        if (hsi[0]<tono_min): 
            tono_min = hsi[0]
            print("minimo")
            print(img2[i][j],tono_min)   

       	if (hsi[0]>tono_max):
            tono_max = hsi[0]
            print("maximo")
            print(img2[i][j],tono_max)
print("tono min - max: ",tono_min,tono_max)

img3 = img.copy()
nhue= 140 #145 dog2, wall 60, mio 180, mandril 140 
n,m,c = img3.shape
for i in range(0,n,1):
    for j in range(0,m,1):
        hsi = calcularRGB2HSI(img3[i][j])
        if ((hsi[0]>=tono_min) and (hsi[0]<=tono_max)):
            #trasnformar de hsb to rgb
            rgb = calcularHSI2RGB([nhue,hsi[1],hsi[2]])
           # print("rgb-hsi2rgb: ",img3[i][j],rgb)
            img3[i][j] = [rgb[0],rgb[1],rgb[2]]
            
cv2.imshow('otroTono',img3 )
cv2.imwrite('otroTono.jpg',img3)
cv2.waitKey(0)

#pix = [[1,255,0]]
#print(cv2.cvtColor(pix,cv2.COLOR_BGR2HSV))
#0<H<360 
#0<S<1 
#0<I<1